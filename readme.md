# Alphanumeric LCD Library #
## For use with LCD display using [Hitachi HD44780U](https://www.pololu.com/file/0J72/HD44780.pdf) hardware controller ##

This library is designed for 8-bit parallel LCDs wired in 4-bit operation mode.

* * *

### INITIALIZING ###

Connections need to be made to the microcontroller for RS, E, and pins DB4 - DB7.  Since this library does not make use of reading data from the display, you should ground pin RW so that it is always in WRITE mode.  The remaining pins are for power and backlight (see [datasheet](https://www.crystalfontz.com/products/document/938/CFAH1602BYYHJT_v1.0.pdf)).

Initialization example:
``` {.cpp}
int8_t RS = 2, E = 3, DB4 = 4, DB5 = 5, DB6 = 6, DB7 = 7; // Configure pinout
LCD Display(RS, E, DB4, DB5, DB6, DB7); // Pass pinout to constructor
```

* * *

### WRITING CHARACTER AND NUMERIC VALUES ###

There are four "write(...)" methods which are used for writing numbers, characters, strings, and also null-terminated character arrays.

WRITING FLOATING POINT AND INTEGER NUMBERS:
~~~ {.cpp}
// Floating point example
const float pi = 3.14159;
// The second argument specifies the precision.
Display.write(pi, 3); // Shows: 3.142

// Integer example (same as float)
int hundred = 100;
Display.write(hundred, 1); // Shows: 100.0

// CAUTION!
int fourtyEight = 48;
Display.write(fourtyEight); // Shows ASCII character '0' because it is interpreted as character since there is no second argument specifying precision.
Display.write(fourtyEight, 0); // Shows: 48
~~~

WRITING CHARACTERS:
~~~ {.cpp}
Display.write('%'); // Shows: %
// There are some built-in ASCII characters that can be accessed like so:
Display.write(LCD::Symbol::Degree); // Shows: °
~~~
Built-in ASCII characters include:

* Degree
* Infinity 
* Omega
* LeftArrow
* RightArrow
* Black

WRITING STRINGS
~~~ {.cpp}
String str = "Hello world";
Display.write(str); // "Hello world"
~~~

WRITING CHARACTER ARRAYS
~~~ {.cpp}
// Char array must be null-terminated
char arr[] = "foo bar"
Display.write(arr); // "foo bar"
~~~

* * *

### CURSOR MANIPULATION ###
~~~ {.cpp}
// Clear display
Display.clear();

// Move cursor
Display.cursor(1, 0); // move cursor down 1 row from top, and indent 0 columns from left.

// Cursor blink or underline
Display.enableBlink(true); // Enables blinking at cursor location
Display.enableUnderline(true); // Enables constant underline at cursor location

// Right and Left text justify:
String label = "Ohms: ";
int ohmValue = 5.1;

Display.cursor(0, 0);
Display.justify(LCD::Left); // Type from left to right
Display.write(label); // "Ohms: [whitespace]"

Display.cursor(0, 15);
Display.justify(LCD::Right); // Type from right to left
Display.write(LCD::Symbol::Omega); // Write symbol first, then value
Display.write(ohmValue, 1); // "[whitespace] 5.1Ω"
// Shows: "Ohms:       5.1Ω"
~~~

* * *

### CREATING CUSTOM CHARACTERS ###

This LCD enables up to 8 user-designed custom characters.  Each character is composed of 8 bytes.  The first byte represents the top row of the dot-matrix of the character.  Given that each row in the matrix is only 5 dots (5 bits), the most significant 3 bits are don't-cares.  For example, the binary value of 00100b tells only the center of the 5 dots in a given row of the dot-matrix to illuminate.

An example of character generation is as follows:
~~~ {.cpp}
char bell[] = {4, 14, 14, 14, 14, 31, 0, 4}; // Each byte, left to right; index 0 = top, 7 = bottom.
// Bell-shape as shown in dot-matrix form
// OOXOO - 4
// OXXXO - 14
// OXXXO - 14
// OXXXO - 14
// OXXXO - 14
// XXXXX - 31
// OOOOO - 0
// OOXOO - 4
char bellIndex = 0; // Index 0 through 7 as there are 8 available spots for custom char's
Display.createChar(bell, bellIndex);
Display.write(bellIndex); // Shows a bell-looking object.
~~~

To assist in visualizing the character generation process, you can use the tool located at the bottom of the page [here](http://www.8051projects.net/lcd-interfacing/lcd-custom-character.php).