#include "LCD.h"

// ESTABLISH USER-SPECIFIED VALUES
// *******************************
LCD::LCD(uint8_t RS, uint8_t E, uint8_t DB4, uint8_t DB5, uint8_t DB6, uint8_t DB7)
{
   init(RS, E, DB4, DB5, DB6, DB7);
};

// INITIALIZE LCD
// **************
void LCD::init(uint8_t RS, uint8_t E, uint8_t DB4, uint8_t DB5, uint8_t DB6, uint8_t DB7)
{
   pin_no[0] = DB4;
   pin_no[1] = DB5;
   pin_no[2] = DB6;
   pin_no[3] = DB7;
   pin_no[4] = RS;
   pin_no[5] = E;
   
   delayMicroseconds(1500); // Datasheet, pg. 46
   
   // Set pins as outputs
   for (char i = 0; i <= 5; i++)
   {
      if (pin_no[i] <= 7)
      {
         DDRD |= 1 << pin_no[i];
      }
      else if (pin_no[i] <= 13)
      {
         DDRB |= 1 << pin_no[i] - 8;
      }
   }
   
   // Initialization
   instr(fourBitInit[0]);
   instr(fourBitInit[1]);
   instr(functionSet);
   instr(displayMode);

   justify(Left);
   clear();
}

// SEND AN INSTRUCTION
// *******************
void LCD::instr(char data)
{
   pin_val[4] = 0; // RS set INSTRUCTION input
   for (int8_t i = 7; i >= 0; i--)
   {
      pin_val[i % 4] = ((data >> i) & 0x01);
      if (i % 4 == 0)
      submit();
   }
   if (data <= 3)
   {
      delayMicroseconds(1520); // 1.52 ms (pg. 47)
   }
   else
   {
      delayMicroseconds(37);   // 37 microseconds
   }
}

// ENABLE BLINKING FEATURE
// ***********************
void LCD::enableBlink(bool enable)
{
   displayMode = (~0x01 & displayMode) | (enable);
   instr(displayMode);
}

// ENABLE UNDERLINE FEATURE
// ************************
void LCD::enableUnderline(bool enable)
{
   displayMode = (~0x01 & displayMode) | (enable << 1);
   instr(displayMode);
}

// CLEAR ENTIRE DISPLAY
// ********************
void LCD::clear(uint8_t charsToClear)
{
   switch (charsToClear)
   {
      case 0:
      {
         instr(clearDisp);
         break;
      }
      default:
      {
         for (uint8_t i = 0; i < charsToClear; i++)
         {
            write(' ');
         }
         break;
      }
   }
}

// MOVE CURSOR
// ***********
void LCD::cursor(int8_t row, int8_t col)
{
   // Text should never be right justified when column is set to zero.
   if ((col == 0) && (justifyText == Right)) justify(Left);
   
   // DDRAM position: Pg. 41
   switch (row)
   {
      case 0:
      {
         instr(DDRAM + col);
         break;
      }
      case 1:
      {
         instr(DDRAM + 0x40 + col);
         break;
      }
      case 2:
      {
         instr(DDRAM + 0x14 + col);
         break;
      }
      case 3:
      {
         instr(DDRAM + 0x54 + col);
         break;
      }
   }
}

// CREATE CUSTOM CHARACTERS
// ************************
void LCD::createChar(CGROM *CGData)
{   
/*
   Usage example:
   LCD::CGRAM Bell {4, 14, 14, 14, 14, 31, 0, 4};
   LCD.createChar(&Bell);
   LCD.write(Bell.index);
*/
   // Automatically assign index to CG character
   static int8_t CGindex = 0;
   CGData->index = (CGindex < 8) ? CGindex++ : 0;

   instr(CGRAM | CGData->index * 8); // Go to CGRAM start address
   justify(Left); // Must increment right as this is submitting DATA not INSTRUCTION
   for (char i = 0; i < 8; i++)
   {
      write(CGData->pattern[i]);
   }
   justify(justifyText); // Restore previous inc right/left setting
   cursor(0, 0); // Cursor location was lost due to character generation index being incremented.
}

// SET Left OR Right TEXT JUSTIFY
// ******************************
void LCD::justify(Justify justify)
{
   justifyText = justify;
   (justify == Right) ? instr(entryMode) : instr(entryMode | 1 << 1);
}

// WRITE A FLOATING POINT NUMBER, DECIMAL PLACES
// *********************************************
void LCD::write(float value, int8_t precision)
{
   char val[16];
   dtostrf(value, 0, precision, val); // Not portable - AVR specific.
   write(val);
}

// WRITE A NULL-TERMINATED CHARACTER ARRAY
// ***************************************
void LCD::write(char str[])
{
   char length = 0;
   while (str[length] != '\0')
   {
      length++;
   };
   
   int8_t i;
   switch (justifyText)
   {
      case Left:
      {
         for (i = 0; i < length; i++)
         {
            write(str[i]);
         }
         break;
      }
      case Right:
      {
         for (i = length - 1; i >= 0; i--)
         {
            write(str[i]);
         }
         break;
      }
   }
}

// WRITE A STRING (REALLY MEMORY EXPENSIVE)
// ****************************************
void LCD::write(String str)
{
   int8_t i;
   int8_t strLen = str.length();
   switch (justifyText)
   {
      case Left:
      {
         for (i = 0; i < strLen; i++)
         {
            write(str.charAt(i));
         }
         break;
      }
      case Right:
      {
         for (i = strLen - 1; i >= 0; i--)
         {
            write(str.charAt(i));
         }
         break;
      }
   }
}

// WRITE A SINGLE CHARACTER OF DATA
// ********************************
void LCD::write(char data)
{
   pin_val[4] = 1; // RS set character input (DATA)
   for (int8_t i = 7; i >= 0; i--)
   {
      pin_val[i % 4] = ((data >> i) & 1);
      if (i % 4 == 0)   
      {
         submit();
      }
   }
   delayMicroseconds(37);
}

// TOGGLE "E" PIN ON LCD
// *********************
void LCD::submit()
{
   // Set RS and the DB4..DB7 nibble
   for (char i = 0; i < 5; i++)
   {
      digitalWrite(pin_no[i], pin_val[i]);
   }

   // Toggle E to submit
   digitalWrite(pin_no[5], 1); // E = HIGH
   digitalWrite(pin_no[5], 0); // E = LOW
}