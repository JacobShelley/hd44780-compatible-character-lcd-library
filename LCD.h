/*
FOR USE WITH HD44780 COMPATABLE LCD CHARACTER DISPLAYS WIRED FOR 4-BIT PARALLEL.
SEE HITACHI HD44780 DATASHEET AT http://fab.cba.mit.edu/classes/863.06/11.13/44780.pdf
*/

#ifndef LCD_H
#define LCD_H
#include <arduino.h> // DDRD, pow, round, abs, PORTx

class LCD
{
// PUBLIC MEMBERS
// **************
public:
   enum Justify
   {
      Right,
      Left
   };
   
   struct Symbol
   {
      static const char Degree =     0xDF;
      static const char Infinity =   0xF3;
      static const char Omega =      0xF4;
      static const char LeftArrow =  0x7F;
      static const char RightArrow = 0x7E;
      static const char Black =      0xFF;
   };
   
   struct CGROM
   {
      char pattern[8];
      uint8_t index;
   };
   
   LCD(uint8_t RS = 2, uint8_t E = 3, uint8_t DB4 = 4, uint8_t DB5 = 5, uint8_t DB6 = 6, uint8_t DB7 = 7);
   void init(uint8_t RS, uint8_t E, uint8_t DB4, uint8_t DB5, uint8_t DB6, uint8_t DB7); // Pinout for LCD
   void clear(uint8_t charsToClear = 0);                                 // Clear entire screen.
   void cursor(int8_t row, int8_t col);          // Move cursor. (0, 0) is first character
   void createChar(CGROM *CGData);               // Record a custom character. Index = 0..7
   void justify(Justify justify);                // Left or Right justify text
   void write(char data);                        // Print a single character
   void write(float inNumber, int8_t precision); // Print number, [decimal] places of precision
   void write(char str[]);                       // Print character array (must be null terminated)
   void write(String str);                       // Print string   
   void enableBlink(bool enable);                // Enable/disable cursor blink
   void enableUnderline(bool enable);            // Enable/disable cursor underline
   
// PRIVATE MEMBERS
// ***************   
private:
   void submit();          // Toggle E to submit the data
   void instr(char data);  // Send instruction
   
   char pin_no[6];      // Dev board pin numbers.
   bool pin_val[5];     // 5 and not 6 as E doesn't store any value - it's just toggled
   Justify justifyText; // Left or right text justify
      
   const char fourBitInit[2] = {0x33, 0x32}; // Per the hitachi HD44780 datasheet, for 4-bit initialization, (figure 24, pg 46)
   const char functionSet =     0x28;  // Function Set,   (1)(8-bit)(2-line)(x[for two disp lines])(x)(x)
   char displayMode =           0x0C;  // Display on/off, (1)(disp on)(cursor on)(blink on)
   const char entryMode =       0x04;  // Entry Set Mode, (1)(cursor right)(shift)
   const char clearDisp =       0x01;  // Clear Display
   const char CGRAM =           0x40;  // Create character in CGRAM
   const char DDRAM =           0x80;  // DDRAM position
};

#endif